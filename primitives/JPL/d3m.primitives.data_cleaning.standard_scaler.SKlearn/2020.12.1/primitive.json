{
  "algorithm_types": [
    "FEATURE_SCALING"
  ],
  "name": "sklearn.preprocessing.data.StandardScaler",
  "primitive_family": "DATA_CLEANING",
  "python_path": "d3m.primitives.data_cleaning.standard_scaler.SKlearn",
  "source": {
    "name": "JPL",
    "contact": "mailto:shah@jpl.nasa.gov",
    "uris": [
      "https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues",
      "https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.StandardScaler.html"
    ]
  },
  "version": "2020.12.1",
  "id": "d639947e-ece0-3a39-a666-e974acf4521d",
  "installation": [
    {
      "type": "PIP",
      "package_uri": "git+https://gitlab.com/datadrivendiscovery/sklearn-wrap.git@627b3b533c3c9ec1fab53bdd54e77b7170712578#egg=sklearn_wrap"
    }
  ],
  "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
  "original_python_path": "sklearn_wrap.SKStandardScaler.SKStandardScaler",
  "primitive_code": {
    "class_type_arguments": {
      "Inputs": "d3m.container.pandas.DataFrame",
      "Outputs": "d3m.container.pandas.DataFrame",
      "Params": "sklearn_wrap.SKStandardScaler.Params",
      "Hyperparams": "sklearn_wrap.SKStandardScaler.Hyperparams"
    },
    "interfaces_version": "2020.11.3",
    "interfaces": [
      "unsupervised_learning.UnsupervisedLearnerPrimitiveBase",
      "base.PrimitiveBase"
    ],
    "hyperparams": {
      "with_mean": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": true,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "If True, center the data before scaling. This does not work (and will raise an exception) when attempted on sparse matrices, because centering them entails building a dense matrix which in common use cases is likely to be too large to fit in memory."
      },
      "with_std": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": true,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "If True, scale the data to unit variance (or equivalently, unit standard deviation)."
      },
      "use_columns": {
        "type": "d3m.metadata.hyperparams.Set",
        "default": [],
        "structural_type": "typing.Sequence[int]",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "A set of column indices to force primitive to operate on. If any specified column cannot be parsed, it is skipped.",
        "elements": {
          "type": "d3m.metadata.hyperparams.Hyperparameter",
          "default": -1,
          "structural_type": "int",
          "semantic_types": []
        },
        "is_configuration": false,
        "min_size": 0
      },
      "exclude_columns": {
        "type": "d3m.metadata.hyperparams.Set",
        "default": [],
        "structural_type": "typing.Sequence[int]",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "A set of column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
        "elements": {
          "type": "d3m.metadata.hyperparams.Hyperparameter",
          "default": -1,
          "structural_type": "int",
          "semantic_types": []
        },
        "is_configuration": false,
        "min_size": 0
      },
      "return_result": {
        "type": "d3m.metadata.hyperparams.Enumeration",
        "default": "new",
        "structural_type": "str",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "Should parsed columns be appended, should they replace original columns, or should only parsed columns be returned? This hyperparam is ignored if use_semantic_types is set to false.",
        "values": [
          "append",
          "replace",
          "new"
        ]
      },
      "use_semantic_types": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": false,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "Controls whether semantic_types metadata will be used for filtering columns in input dataframe. Setting this to false makes the code ignore return_result and will produce only the output dataframe"
      },
      "add_index_columns": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": false,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\"."
      },
      "error_on_no_input": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": true,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "Throw an exception if no input column is selected/provided. Defaults to true to behave like sklearn. To prevent pipelines from breaking set this to False."
      },
      "return_semantic_type": {
        "type": "d3m.metadata.hyperparams.Enumeration",
        "default": "https://metadata.datadrivendiscovery.org/types/Attribute",
        "structural_type": "str",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "Decides what semantic type to attach to generated attributes",
        "values": [
          "https://metadata.datadrivendiscovery.org/types/Attribute",
          "https://metadata.datadrivendiscovery.org/types/ConstructedAttribute"
        ]
      }
    },
    "arguments": {
      "hyperparams": {
        "type": "sklearn_wrap.SKStandardScaler.Hyperparams",
        "kind": "RUNTIME"
      },
      "random_seed": {
        "type": "int",
        "kind": "RUNTIME",
        "default": 0
      },
      "docker_containers": {
        "type": "typing.Union[NoneType, typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]]",
        "kind": "RUNTIME",
        "default": null
      },
      "timeout": {
        "type": "typing.Union[NoneType, float]",
        "kind": "RUNTIME",
        "default": null
      },
      "iterations": {
        "type": "typing.Union[NoneType, int]",
        "kind": "RUNTIME",
        "default": null
      },
      "produce_methods": {
        "type": "typing.Sequence[str]",
        "kind": "RUNTIME"
      },
      "inputs": {
        "type": "d3m.container.pandas.DataFrame",
        "kind": "PIPELINE"
      },
      "params": {
        "type": "sklearn_wrap.SKStandardScaler.Params",
        "kind": "RUNTIME"
      }
    },
    "class_methods": {},
    "instance_methods": {
      "__init__": {
        "kind": "OTHER",
        "arguments": [
          "hyperparams",
          "random_seed",
          "docker_containers"
        ],
        "returns": "NoneType"
      },
      "fit": {
        "kind": "OTHER",
        "arguments": [
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
        "description": "Fits primitive using inputs and outputs (if any) using currently set training data.\n\nThe returned value should be a ``CallResult`` object with ``value`` set to ``None``.\n\nIf ``fit`` has already been called in the past on different training data,\nthis method fits it **again from scratch** using currently set training data.\n\nOn the other hand, caller can call ``fit`` multiple times on the same training data\nto continue fitting.\n\nIf ``fit`` fully fits using provided training data, there is no point in making further\ncalls to this method with same training data, and in fact further calls can be noops,\nor a primitive can decide to fully refit from scratch.\n\nIn the case fitting can continue with same training data (even if it is maybe not reasonable,\nbecause the internal metric primitive is using looks like fitting will be degrading), if ``fit``\nis called again (without setting training data), the primitive has to continue fitting.\n\nCaller can provide ``timeout`` information to guide the length of the fitting process.\nIdeally, a primitive should adapt its fitting process to try to do the best fitting possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore fitting, it should raise a ``TimeoutError`` exception to signal that fitting was\nunsuccessful in the given time. The state of the primitive after the exception should be\nas the method call has never happened and primitive should continue to operate normally.\nThe purpose of ``timeout`` is to give opportunity to a primitive to cleanly manage\nits state instead of interrupting execution from outside. Maintaining stable internal state\nshould have precedence over respecting the ``timeout`` (caller can terminate the misbehaving\nprimitive from outside anyway). If a longer ``timeout`` would produce different fitting,\nthen ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal fitting iterations (for example, epochs). For those, caller\ncan provide how many of primitive's internal iterations should a primitive do before returning.\nPrimitives should make iterations as small as reasonable. If ``iterations`` is ``None``,\nthen there is no limit on how many iterations the primitive should do and primitive should\nchoose the best amount of iterations on its own (potentially controlled through\nhyper-parameters). If ``iterations`` is a number, a primitive has to do those number of\niterations (even if not reasonable), if possible. ``timeout`` should still be respected\nand potentially less iterations can be done because of that. Primitives with internal\niterations should make ``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should fit fully, respecting only ``timeout``.\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value."
      },
      "fit_multi_produce": {
        "kind": "OTHER",
        "arguments": [
          "produce_methods",
          "inputs",
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.MultiCallResult",
        "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to ``set_training_data`` and all produce methods.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
      },
      "get_params": {
        "kind": "OTHER",
        "arguments": [],
        "returns": "sklearn_wrap.SKStandardScaler.Params",
        "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nAn instance of parameters."
      },
      "multi_produce": {
        "kind": "OTHER",
        "arguments": [
          "produce_methods",
          "inputs",
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.MultiCallResult",
        "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults. Despite accepting all arguments they can be passed as ``None`` by the caller\nwhen they are not needed by any of the produce methods in ``produce_methods``.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
      },
      "produce": {
        "kind": "PRODUCE",
        "arguments": [
          "inputs",
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
        "singleton": false,
        "inputs_across_samples": [],
        "description": "Produce primitive's best choice of the output for each of the inputs.\n\nThe output value should be wrapped inside ``CallResult`` object before returning.\n\nIn many cases producing an output is a quick operation in comparison with ``fit``, but not\nall cases are like that. For example, a primitive can start a potentially long optimization\nprocess to compute outputs. ``timeout`` and ``iterations`` can serve as a way for a caller\nto guide the length of this process.\n\nIdeally, a primitive should adapt its call to try to produce the best outputs possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore producing outputs, it should raise a ``TimeoutError`` exception to signal that the\ncall was unsuccessful in the given time. The state of the primitive after the exception\nshould be as the method call has never happened and primitive should continue to operate\nnormally. The purpose of ``timeout`` is to give opportunity to a primitive to cleanly\nmanage its state instead of interrupting execution from outside. Maintaining stable internal\nstate should have precedence over respecting the ``timeout`` (caller can terminate the\nmisbehaving primitive from outside anyway). If a longer ``timeout`` would produce\ndifferent outputs, then ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal iterations (for example, optimization iterations).\nFor those, caller can provide how many of primitive's internal iterations\nshould a primitive do before returning outputs. Primitives should make iterations as\nsmall as reasonable. If ``iterations`` is ``None``, then there is no limit on\nhow many iterations the primitive should do and primitive should choose the best amount\nof iterations on its own (potentially controlled through hyper-parameters).\nIf ``iterations`` is a number, a primitive has to do those number of iterations,\nif possible. ``timeout`` should still be respected and potentially less iterations\ncan be done because of that. Primitives with internal iterations should make\n``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should run fully, respecting only ``timeout``.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
      },
      "set_params": {
        "kind": "OTHER",
        "arguments": [
          "params"
        ],
        "returns": "NoneType",
        "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams:\n    An instance of parameters."
      },
      "set_training_data": {
        "kind": "OTHER",
        "arguments": [
          "inputs"
        ],
        "returns": "NoneType",
        "description": "Sets training data of this primitive.\n\nParameters\n----------\ninputs:\n    The inputs."
      }
    },
    "class_attributes": {
      "logger": "logging.Logger",
      "metadata": "d3m.metadata.base.PrimitiveMetadata"
    },
    "instance_attributes": {
      "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
      "random_seed": "int",
      "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
      "volumes": "typing.Dict[str, str]",
      "temporary_directory": "typing.Union[NoneType, str]"
    },
    "params": {
      "scale_": "typing.Union[NoneType, numpy.ndarray]",
      "mean_": "typing.Union[NoneType, numpy.ndarray]",
      "var_": "typing.Union[NoneType, numpy.ndarray]",
      "n_samples_seen_": "typing.Union[NoneType, int, numpy.integer]",
      "input_column_names": "typing.Union[NoneType, pandas.core.indexes.base.Index]",
      "target_names_": "typing.Union[NoneType, typing.Sequence[typing.Any]]",
      "training_indices_": "typing.Union[NoneType, typing.Sequence[int]]",
      "target_column_indices_": "typing.Union[NoneType, typing.Sequence[int]]",
      "target_columns_metadata_": "typing.Union[NoneType, typing.List[collections.OrderedDict]]"
    }
  },
  "structural_type": "sklearn_wrap.SKStandardScaler.SKStandardScaler",
  "description": "Primitive wrapping for sklearn StandardScaler\n`sklearn documentation <https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.StandardScaler.html>`_\n\nAttributes\n----------\nmetadata:\n    Primitive's metadata. Available as a class attribute.\nlogger:\n    Primitive's logger. Available as a class attribute.\nhyperparams:\n    Hyperparams passed to the constructor.\nrandom_seed:\n    Random seed passed to the constructor.\ndocker_containers:\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes:\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory:\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
  "digest": "98bb04a9e703af4ad0743b92547f14dbf137fa05cc69d67c45d2e493e4aac950"
}