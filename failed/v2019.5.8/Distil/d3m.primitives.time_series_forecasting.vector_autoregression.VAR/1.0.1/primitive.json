{
    "id": "76b5a479-c209-4d94-92b5-7eba7a4d4499",
    "version": "1.0.1",
    "name": "VAR",
    "keywords": [
        "Time Series"
    ],
    "source": {
        "name": "Distil",
        "contact": "mailto:nklabs@newknowledge.com",
        "uris": [
            "https://github.com/NewKnowledge/TimeSeries-D3M-Wrappers"
        ]
    },
    "installation": [
        {
            "type": "PIP",
            "package": "cython",
            "version": "0.29.7"
        },
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/NewKnowledge/TimeSeries-D3M-Wrappers.git@b7754221711e18a64d23477e5508acc5e421aae3#egg=TimeSeriesD3MWrappers"
        }
    ],
    "python_path": "d3m.primitives.time_series_forecasting.vector_autoregression.VAR",
    "algorithm_types": [
        "VECTOR_AUTOREGRESSION"
    ],
    "primitive_family": "TIME_SERIES_FORECASTING",
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "TimeSeriesD3MWrappers.VAR.VAR",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "TimeSeriesD3MWrappers.VAR.Params",
            "Hyperparams": "TimeSeriesD3MWrappers.VAR.Hyperparams"
        },
        "interfaces_version": "2019.5.8",
        "interfaces": [
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "datetime_index": {
                "type": "d3m.metadata.hyperparams.Set",
                "default": [],
                "structural_type": "typing.Sequence[int]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "if multiple datetime indices exist, this HP specifies which to apply to training data. If             None, the primitive assumes there is only one datetime index. This HP can also specify multiple indices             which should be concatenated to form datetime_index",
                "elements": {
                    "type": "d3m.metadata.hyperparams.Hyperparameter",
                    "default": -1,
                    "structural_type": "int",
                    "semantic_types": []
                },
                "is_configuration": false,
                "min_size": 0
            },
            "datetime_index_unit": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": null,
                "structural_type": "typing.Union[NoneType, str]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "unit of the datetime column if datetime column is integer or float"
            },
            "filter_index_one": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": null,
                "structural_type": "typing.Union[NoneType, int]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "top-level index of column in input dataset that contain unique identifiers of different time series"
            },
            "filter_index_two": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": null,
                "structural_type": "typing.Union[NoneType, int]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "second-level index of column in input dataset that contain unique identifiers of different time series"
            },
            "n_periods": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 61,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "number of periods to predict",
                "lower": 1,
                "upper": 9223372036854775807,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "interval": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": null,
                "structural_type": "typing.Union[NoneType, int]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "interval with which to sample future predictions"
            },
            "datetime_interval_exception": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": null,
                "structural_type": "typing.Union[NoneType, str]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "to handle different prediction intervals (stock market dataset).             If this HP is set, primitive will just make next forecast for this datetime value             (not multiple forecasts at multiple intervals"
            },
            "max_lags": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 10,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "maximum lag order to evluate to find model - eval criterion = AIC",
                "lower": 1,
                "upper": 9223372036854775807,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "arma_p": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 0,
                "structural_type": "typing.Union[NoneType, int]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "The p order of the ARMA model in case some time series are univariate"
            },
            "arma_q": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 0,
                "structural_type": "typing.Union[NoneType, int]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "The q order of the ARMA model in case some time series are univariate"
            },
            "weights_filter_value": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": null,
                "structural_type": "typing.Union[NoneType, str]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "value to select a filter from column filter index for which to return correlation              coefficient matrix."
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "TimeSeriesD3MWrappers.VAR.Hyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "outputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "TimeSeriesD3MWrappers.VAR.Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {
            "can_accept": {
                "arguments": {
                    "method_name": {
                        "type": "str"
                    },
                    "arguments": {
                        "type": "typing.Dict[str, typing.Union[d3m.metadata.base.Metadata, type]]"
                    },
                    "hyperparams": {
                        "type": "TimeSeriesD3MWrappers.VAR.Hyperparams"
                    }
                },
                "returns": "typing.Union[NoneType, d3m.metadata.base.DataMetadata]",
                "description": "Returns a metadata object describing the output of a call of ``method_name`` method under\n``hyperparams`` with primitive arguments ``arguments``, if such arguments can be accepted by the method.\nOtherwise it returns ``None`` or raises an exception.\n\nDefault implementation checks structural types of ``arguments`` expected arguments' types\nand ignores ``hyperparams``.\n\nBy (re)implementing this method, a primitive can fine-tune which arguments it accepts\nfor its methods which goes beyond just structural type checking. For example, a primitive might\noperate only on images, so it can accept numpy arrays, but only those with semantic type\ncorresponding to an image. Or it might check dimensions of an array to assure it operates\non square matrix.\n\nPrimitive arguments are a superset of method arguments. This method receives primitive arguments and\nnot just method arguments so that it is possible to implement it without a state between calls\nto ``can_accept`` for multiple methods. For example, a call to ``fit`` could during normal execution\ninfluences what a later ``produce`` call outputs. But during ``can_accept`` call we can directly have\naccess to arguments which would have been given to ``fit`` to produce metadata of the ``produce`` call.\n\nNot all primitive arguments have to be provided, only those used by ``fit``, ``set_training_data``,\nand produce methods, and those used by the ``method_name`` method itself.\n\nParameters\n----------\nmethod_name : str\n    Name of the method which would be called.\narguments : Dict[str, Union[Metadata, type]]\n    A mapping between argument names and their metadata objects (for pipeline arguments) or types (for other).\nhyperparams : Hyperparams\n    Hyper-parameters under which the method would be called during regular primitive execution.\n\nReturns\n-------\nDataMetadata\n    Metadata object of the method call result, or ``None`` if arguments are not accepted\n    by the method."
            }
        },
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed"
                ],
                "returns": "NoneType"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "fits VAR model. Evaluates different lag orders up to maxlags, eval criterion = AIC\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "outputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nThis method allows primitive author to implement an optimized version of both fitting\nand producing a primitive on same data.\n\nIf any additional method arguments are added to primitive's ``set_training_data`` method\nor produce method(s), or removed from them, they have to be added to or removed from this\nmethod as well. This method should accept an union of all arguments accepted by primitive's\n``set_training_data`` method and produce method(s) and then use them accordingly when\ncomputing results.\n\nThe default implementation of this method just calls first ``set_training_data`` method,\n``fit`` method, and all produce methods listed in ``produce_methods`` in order and is\npotentially inefficient.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to ``set_training_data`` and all produce methods.\noutputs : Outputs\n    The outputs given to ``set_training_data``.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "TimeSeriesD3MWrappers.VAR.Params",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nParams\n    An instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Produce primitive's prediction for future time series data\n\nParameters\n----------\nNone\n\nReturns\n-------\nOutputs\n    The output is a data frame containing the d3m index and a forecast for each of the 'n_periods' future time periods,\n    modified if desired by the 'interval' HP\n    The default is a future forecast for each of the selected input variables. This can be modified to just one output\n        variable with the associated HP"
            },
            "produce_weights": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Produce correlation coefficients (weights) for each of the terms used in the regression model\n\nParameters\n----------\nfilter_value:   value to select a filter from column filter index for which to return correlation coefficient matrix. If None, \n                method returns most recent filter\n\nReturns\n----------\nOutputs\n    The output is a data frame containing columns for each of the terms used in the regression model. Each row contains the \n    correlation coefficients for each term in the regression model. If there are multiple unique timeseries indices in the \n    dataset there can be multiple rows in this output dataset. Terms that aren't included in a specific timeseries index will \n    have a value of NA in the associated matrix entry."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs",
                    "outputs"
                ],
                "returns": "NoneType",
                "description": "Sets primitive's training data\n\nParameters\n----------\ninputs: input d3m_dataframe containing n columns of features"
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {}
    },
    "structural_type": "TimeSeriesD3MWrappers.VAR.VAR",
    "description": "Produce primitive's prediction for future time series data. The output is a data frame containing the d3m index and a\nforecast for each of the 'n_periods' future time periods, modified if desired by the 'interval' HP. The default is a\nfuture forecast for each of the selected input variables. This can be modified to just one output variable with\nthe associated HP\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "digest": "f14c9e2804affdb14d8a71cb21dfb26e6eba8c0da22faf05094bacc1056dd889"
}
