{
    "id": "7288e169-5c2b-434a-96f8-cb2144e7f9cc",
    "version": "0.1.0",
    "name": "Bayesian Linear Regression",
    "description": "-------------\nInputs:  DataFrame of features of shape: NxM, where N = samples and M = features.\nOutputs: DataFrame containing the target column of shape Nx1 or denormalized dataset.\n-------------\n\nAttributes\n----------\nmetadata:\n    Primitive's metadata. Available as a class attribute.\nlogger:\n    Primitive's logger. Available as a class attribute.\nhyperparams:\n    Hyperparams passed to the constructor.\nrandom_seed:\n    Random seed passed to the constructor.\ndocker_containers:\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes:\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory:\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "python_path": "d3m.primitives.regression.linear_regression.UBC",
    "primitive_family": "REGRESSION",
    "algorithm_types": [
        "LINEAR_REGRESSION"
    ],
    "source": {
        "name": "UBC",
        "contact": "mailto:tonyjos@ubc.cs.ca",
        "uris": [
            "https://github.com/plai-group/ubc_primitives.git"
        ]
    },
    "keywords": [
        "bayesian",
        "regression"
    ],
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/plai-group/ubc_primitives.git@f8f66a36264e2f263a3156a80182d0a27449c600#egg=ubc_primitives"
        }
    ],
    "hyperparams_to_tune": [
        "learning_rate",
        "minibatch_size"
    ],
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "primitives_ubc.linearRegression.linear_regression.LinearRegressionPrimitive",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "primitives_ubc.linearRegression.linear_regression.Params",
            "Hyperparams": "primitives_ubc.linearRegression.linear_regression.Hyperparams"
        },
        "interfaces_version": "2020.5.18",
        "interfaces": [
            "base.ProbabilisticCompositionalityMixin",
            "base.GradientCompositionalityMixin",
            "base.SamplingCompositionalityMixin",
            "supervised_learning.SupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "learning_rate": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 0.0001,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Learning rate used during training (fit)."
            },
            "weights_prior": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": null,
                "structural_type": "typing.Union[NoneType, d3m.primitive_interfaces.base.GradientCompositionalityMixin]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "prior on weights"
            },
            "tune_prior_end_to_end": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": false,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "setting this to true will case the end to end training to propogate                     back to the prior parameters. what this means is that the actual                     parameters of the prior distribution are going to be changed                    according to the chain rule."
            },
            "use_gradient_fit": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": false,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Use gradient fit instead of analytic fit."
            },
            "analytic_fit_threshold": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 100,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "the threshold used for N/P where n is the number of training data,                     and P is the number of features. The training matrix X is likely to be                     rank deficient when N >> P or when P > N. If these are both not the case                     i.e. the matrix is full rank, we can use the analytic version.                     You can also force this primitive to use gradient optimization                     by setting this to zero"
            },
            "minibatch_size": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 100,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Minibatch size used during training (fit), if using gradient fit"
            },
            "num_iterations": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 100,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Number of iterations to train the model."
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "primitives_ubc.linearRegression.linear_regression.Hyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "gradient_outputs": {
                "type": "d3m.primitive_interfaces.base.Gradients[d3m.container.pandas.DataFrame]",
                "kind": "RUNTIME"
            },
            "fine_tune": {
                "type": "bool",
                "kind": "RUNTIME",
                "default": false
            },
            "fine_tune_learning_rate": {
                "type": "float",
                "kind": "RUNTIME",
                "default": 1e-05
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "outputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "num_samples": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 1
            },
            "temperature": {
                "type": "float",
                "kind": "RUNTIME",
                "default": 0
            },
            "params": {
                "type": "primitives_ubc.linearRegression.linear_regression.Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed"
                ],
                "returns": "NoneType"
            },
            "backward": {
                "kind": "OTHER",
                "arguments": [
                    "gradient_outputs",
                    "fine_tune",
                    "fine_tune_learning_rate"
                ],
                "returns": "typing.Tuple[d3m.primitive_interfaces.base.Gradients[d3m.container.pandas.DataFrame], d3m.primitive_interfaces.base.Gradients[primitives_ubc.linearRegression.linear_regression.Params]]",
                "description": "Returns the gradient with respect to inputs and with respect to params of a loss\nthat is being backpropagated end-to-end in a pipeline.\n\nThis is the standard backpropagation algorithm: backpropagation needs to be preceded by a\nforward propagation (``forward`` method call).\n\nParameters\n----------\ngradient_outputs:\n    The gradient of the loss with respect to this primitive's output. During backpropagation,\n    this comes from the next primitive in the pipeline, i.e., the primitive whose input\n    is the output of this primitive during the forward execution with ``forward`` (and ``produce``).\nfine_tune:\n    If ``True``, executes a fine-tuning gradient descent step as a part of this call.\n    This provides the most straightforward way of end-to-end training/fine-tuning.\nfine_tune_learning_rate:\n    Learning rate for end-to-end training/fine-tuning gradient descent steps.\nfine_tune_weight_decay:\n    L2 regularization (weight decay) coefficient for end-to-end training/fine-tuning gradient\n    descent steps.\n\nReturns\n-------\nA tuple of the gradient with respect to inputs and with respect to params."
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult",
                "description": "Runs gradient descent for ``timeout`` seconds or ``iterations``\niterations, whichever comes sooner, on log normal_density(self.weights * self.input\n- output, identity*self.noise_variance) +\nparameter_prior_primitives[\"weights\"].score(self.weights) +\nparameter_prior_primitives[\"noise_variance\"].score(noise_variance).\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "outputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nThis method allows primitive author to implement an optimized version of both fitting\nand producing a primitive on same data.\n\nIf any additional method arguments are added to primitive's ``set_training_data`` method\nor produce method(s), or removed from them, they have to be added to or removed from this\nmethod as well. This method should accept an union of all arguments accepted by primitive's\n``set_training_data`` method and produce method(s) and then use them accordingly when\ncomputing results.\n\nThe default implementation of this method just calls first ``set_training_data`` method,\n``fit`` method, and all produce methods listed in ``produce_methods`` in order and is\npotentially inefficient.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to ``set_training_data`` and all produce methods.\noutputs:\n    The outputs given to ``set_training_data``.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "forward": {
                "kind": "OTHER",
                "arguments": [
                    "inputs"
                ],
                "returns": "d3m.container.pandas.DataFrame",
                "description": "Similar to ``produce`` method but it is meant to be used for a forward pass during\nbackpropagation-based end-to-end training. Primitive can implement it differently\nthan ``produce``, e.g., forward pass during training can enable dropout layers, or\n``produce`` might not compute gradients while ``forward`` does.\n\nBy default it calls ``produce`` for one iteration.\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\n\nReturns\n-------\nThe outputs of shape [num_inputs, ...]."
            },
            "get_call_metadata": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "d3m.primitive_interfaces.base.CallResult"
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "primitives_ubc.linearRegression.linear_regression.Params",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nAn instance of parameters."
            },
            "gradient_output": {
                "kind": "OTHER",
                "arguments": [
                    "outputs",
                    "inputs"
                ],
                "returns": "d3m.primitive_interfaces.base.Gradients[d3m.container.pandas.DataFrame]",
                "description": "Calculates grad_output log normal_density(self.weights * self.input - output, identity * self.noise_variance)\nfor a single input/output pair.\n\nParameters\n----------\noutputs:\n    The outputs.\ninputs:\n    The inputs.\n\nReturns\n-------\nA structure similar to ``Container`` but the values are of type ``Optional[float]``."
            },
            "gradient_params": {
                "kind": "OTHER",
                "arguments": [
                    "outputs",
                    "inputs"
                ],
                "returns": "d3m.primitive_interfaces.base.Gradients[primitives_ubc.linearRegression.linear_regression.Params]",
                "description": "Calculates grad_weights fit_term_temperature *\nlog normal_density(self.weights * self.input - output, identity * self.noise_variance)\nfor a single input/output pair.\n\nParameters\n----------\noutputs:\n    The outputs.\ninputs:\n    The inputs.\n\nReturns\n-------\nA version of ``Params`` with all differentiable fields from ``Params`` and values set to gradient for each parameter."
            },
            "log_likelihood": {
                "kind": "OTHER",
                "arguments": [
                    "outputs",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[float]",
                "description": "Returns log probability of outputs given inputs and params under this primitive:\n\nsum_i(log(p(output_i | input_i, params)))\n\nBy default it calls ``log_likelihoods`` and tries to automatically compute a sum, but subclasses can\nimplement a more efficient or even correct version.\n\nParameters\n----------\noutputs:\n    The outputs. The number of samples should match ``inputs``.\ninputs:\n    The inputs. The number of samples should match ``outputs``.\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nsum_i(log(p(output_i | input_i, params))) wrapped inside ``CallResult``.\nThe number of returned samples is always 1.\nThe number of columns should match the number of target columns in ``outputs``."
            },
            "log_likelihoods": {
                "kind": "OTHER",
                "arguments": [
                    "outputs",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.numpy.ndarray]",
                "description": "input : D-length numpy ndarray\noutput : float\nCalculates\nlog(normal_density(self.weights * self.input - output, identity * self.noise_variance))\nfor a single input/output pair.\n\nParameters\n----------\noutputs:\n    The outputs. The number of samples should match ``inputs``.\ninputs:\n    The inputs. The number of samples should match ``outputs``.\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nlog(p(output_i | input_i, params))) wrapped inside ``CallResult``.\nThe number of columns should match the number of target columns in ``outputs``."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "inputs: (num_inputs,  D) numpy array\noutputs : numpy array of dimension (num_inputs)\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "sample": {
                "kind": "OTHER",
                "arguments": [
                    "inputs",
                    "num_samples",
                    "timeout",
                    "iterations"
                ],
                "returns": "typing.Sequence[d3m.container.pandas.DataFrame]",
                "description": "input : num_inputs x D numpy ndarray\noutputs : num_predictions x num_inputs numpy ndarray\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\nnum_samples:\n    The number of samples to return in a set of samples.\ntimeout:\n    A maximum time this primitive should take to sample outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe multiple sets of samples of shape [num_samples, num_inputs, ...] wrapped inside\n``CallResult``. While the output value type is specified as ``Sequence[Outputs]``, the\noutput value can be in fact any container type with dimensions/shape equal to combined\n``Sequence[Outputs]`` dimensions/shape. Subclasses should specify which exactly type\nthe output is."
            },
            "set_fit_term_temperature": {
                "kind": "OTHER",
                "arguments": [
                    "temperature"
                ],
                "returns": "NoneType",
                "description": "Sets the temperature used in ``gradient_output`` and ``gradient_params``.\n\nParameters\n----------\ntemperature:\n    The temperature to use, [0, inf), typically, [0, 1]."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams:\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs",
                    "outputs"
                ],
                "returns": "NoneType",
                "description": "Sets current training data of this primitive.\n\nThis marks training data as changed even if new training data is the same as\nprevious training data.\n\nStandard sublasses in this package do not adhere to the Liskov substitution principle when\ninheriting this method because they do not necessary accept all arguments found in the base\nclass. This means that one has to inspect which arguments are accepted at runtime, or in\nother words, one has to inspect which exactly subclass a primitive implements, if\nyou are accepting a wider range of primitives. This relaxation is allowed only for\nstandard subclasses found in this package. Primitives themselves should not break\nthe Liskov substitution principle but should inherit from a suitable base class.\n\nParameters\n----------\ninputs:\n    The inputs.\noutputs:\n    The outputs."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "weights": "d3m.container.numpy.ndarray",
            "weights_variance": "d3m.container.numpy.ndarray",
            "offset": "float",
            "noise_variance": "float",
            "target_names_": "typing.Union[NoneType, typing.List[str]]"
        }
    },
    "structural_type": "primitives_ubc.linearRegression.linear_regression.LinearRegressionPrimitive",
    "digest": "ba769848978a95bb1a9626454195b607816f3f892547d4d709eadf71bfb5fabe"
}
